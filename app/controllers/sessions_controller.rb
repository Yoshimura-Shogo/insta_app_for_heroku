class SessionsController < ApplicationController
  def new
  end

  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    if @user&.authenticate(params[:session][:password])
      login @user
      flash[:success] = "ログインしました"
      redirect_to @user
    else
      flash.now[:danger] = "メールアドレスかパスワードが正しくありません"
      render 'new'
    end
  end

  def destroy
    logout
    flash[:info] = "ログアウトしました"
    redirect_to root_url
  end

  def facebook_login
    @user = User.from_omniauth(request.env["omniauth.auth"])
    result = @user.save(context: :facebook_login)
    if result
      login @user
      redirect_to @user
    else
      flash[:danger] = "ログインに失敗しました"
      redirect_to auth_failure_path
    end
  end

  def auth_failure
    @user = User.new
    render 'top_pages/home'
  end
end
