class TopPagesController < ApplicationController
  def home
    if logged_in?
      @feed_items = current_user.feed.paginate(page: params[:page])
      @users = current_user.following.paginate(page: params[:page])
    end
  end

  def terms
  end
end
