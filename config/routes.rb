Rails.application.routes.draw do
  root 'top_pages#home'
  get '/terms', to: 'top_pages#terms'
  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get '/auth/:provider/callback', to: 'sessions#facebook_login', as: :auth_callback
  get '/auth/failure', to: 'sessions#auth_failure', as: :auth_failure
  resources :users do
    member do
      get :edit_password, :following, :followers
      patch :update_password
    end
  end
  resources :microposts, only: [:create, :destroy, :new, :show] do
    resources :comments
  end
  resources :relationships, only: [:create, :destroy]
end
