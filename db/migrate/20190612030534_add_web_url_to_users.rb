class AddWebUrlToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :web_url, :string
  end
end
