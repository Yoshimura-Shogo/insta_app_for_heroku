ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/reporters'
Minitest::Reporters.use!

class ActiveSupport::TestCase
  fixtures :all
  include ApplicationHelper

#テストユーザがログインしていればtrueを返す
  def is_logged_in?
    !session[:user_id].nil?
  end

#テストユーザでログイン
  def log_in_as(user)
    post login_path, params: { session: { email: user.email,
                              password:'password' } }
  end
end
