require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  test "invalid user sign up" do
    get signup_path
    assert_no_difference 'User.count' do
      post signup_path, params: { user: { name: "", email: "foo@invalid",
                                  password: "foo",
                                  password_confirmation: "bar" } }
    end
    assert_template 'users/new'
    assert_select 'div#error_explanation'
    assert_select 'div.alert-danger'
  end

  test "valid user sign up" do
    get signup_path
    assert_difference 'User.count', 1 do
      post signup_path, params: { user: { name: "User", user_name: "user",
                                  email: "foo@valid.com",
                                  password: "foobar",
                                  password_confirmation: "foobar" } }
    end
    assert_not flash.empty?
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
  end
end
