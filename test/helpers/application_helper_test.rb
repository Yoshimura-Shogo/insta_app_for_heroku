require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full_title helper" do
    assert_equal 'Insta_app', full_title
  end
end
